package main

import (
	"fmt"
	"math"
	"math/rand"
	"strconv"
)

func add_2numbers(a, b int) int {
	return a + b
}

//returns second and first strings
func str_swap(x, y string) (string, string) {
	return y, x
}

//package level variable(s that will be used for a long time should have verbose naming)
var verbose int = 15

//global level variables have to start with an uppercase letter
var More_verbose string = "ok"

func main() {
	fmt.Println("Hello Go")
	fmt.Println("My favourite number is", rand.Intn(10))
	fmt.Println("\tPrintln: Now you have", math.Sqrt(7), "problems.")
	fmt.Printf("\tPrintf: Now you have %g problems.\n", math.Sqrt(7))

	//shadowing a package level variable with the innermost declared variable of the same name
	verbose := 5 //initializes the variable and lets the compiler decide what type it is
	fmt.Printf("variable %v, type %T\n", verbose, verbose)
	fmt.Println(", + 20 =", add_2numbers(verbose, 20))

	s1, s2 := str_swap("Cat", "Mouse")
	fmt.Printf("%s eat %s\n", s1, s2)

	var i float32 = 42.99
	fmt.Printf("\n%v, %T\n", i, i)

	j := int(i)
	fmt.Printf("%v, %T\n", j, j)

	//prints out the Unicode character of given value (#42 in the Unicode table is an asterisk - '*')
	k := string(j)
	fmt.Printf("string():\t%v, %T\n", k, k)

	k = fmt.Sprint(i)
	fmt.Printf("fmt.Sprint():\t%v, %T\n", k, k)

	l := strconv.Itoa(j)
	fmt.Printf("strconv.Itoa():\t%v, %T\n", l, l)

}
